define(function() {
    var internals = {};
    var externals = {};

    internals.elements = {};
    internals.handlers = {};

    internals.renderPokemon = function(pokemon) {
        if(internals.elements.pokemonCard) {
            internals.elements.img.remove();
        }
        internals.elements.pokePic = $('#picture');
        internals.elements.pokemonCard = $(internals.createPokemonCard(pokemon));
        internals.elements.pokePic.append(internals.elements.pokemonCard);
    };

    internals.renderButtons = function() {
        if(internals.elements.nextPokeButton) {
            return;
        }

        internals.elements.nextPokeButton = $(internals.createNextPokeButton());
        internals.elements.nextPokeButton.click(internals.handlers["buttonClick"]);
        internals.elements.app.append(internals.elements.nextPokeButton);

        internals.elements.detailsButton = $(internals.createDetailsButton());
        internals.elements.detailsButton.click(internals.handlers["detailsClick"]);
        internals.elements.app.append(internals.elements.detailsButton);
    };

    internals.renderPokedex = function() {
        if(internals.elements.pokedex) {
          console.log("im hereeeeee@@@")
            return;
        }
        internals.elements.pokedex = $(internals.createPokedex());
        internals.elements.pokedexImg.append(internals.elements.pokedex);
    }

    externals.render = function(error, pokemon) {
        internals.elements.app = $('#app');
        internals.elements.img = $('img');
        internals.elements.pokedexImg = $('#pokedex');

        internals.renderButtons();
        internals.renderPokedex();

        if (pokemon) {
            internals.renderPokemon(pokemon);
        }

    };

    externals.bind = function(event, handler) {
        internals.handlers[event] = handler;
    };

//String clutter here   TO-DO LIST!
    internals.createPokemonCard = function(pokemon) {
        console.log('creating pokémon');
        return '<img src=\"' + pokemon.sprite + '\" height=\"170\">';
    };

    internals.createNextPokeButton = function() {
        return "<button> Click me for the next pokemon!</button>";
    }

    internals.createDetailsButton = function() {
        return "<button> Click me for details!</button>";
    }

    internals.createPokedex = function() {
        console.log('creating pokedex');
        return '<div id="left">' +
        '<div id="logo"></div>' +
        '<div id="bg_curve1_left"></div>' +
        '<div id="bg_curve2_left"></div>' +
        '<div id="curve1_left">' +
          '<div id="buttonGlass">' +
            '<div id="reflect"> </div>' +
          '</div>' +
          '<div id="miniButtonGlass1"></div>' +
          '<div id="miniButtonGlass2"></div>' +
          '<div id="miniButtonGlass3"></div>' +
        '</div>' +
        '<div id="curve2_left">' +
          '<div id="junction">' +
            '<div id="junction1"></div>' +
            '<div id="junction2"></div>' +
          '</div>' +
        '</div>' +
        '<div id="screen">' +
          '<div id="topPicture">' +
            '<div id="buttontopPicture1"></div>' +
            '<div id="buttontopPicture2"></div>' +
          '</div>' +
          '<div id="picture">' +
          '</div>' +
          '<div id="buttonbottomPicture"></div>' +
          '<div id="speakers">' +
            '<div class="sp"></div>' +
            '<div class="sp"></div>' +
            '<div class="sp"></div>' +
            '<div class="sp"></div>' +
          '</div>' +
        '</div>'+
        '<div id="bigbluebutton"></div>' +
        '<div id="barbutton1"></div>' +
        '<div id="barbutton2"></div>' +
        '<div id="cross">' +
          '<div id="leftcross">' +
            '<div id="leftT"></div>' +
          '</div>' +
          '<div id="topcross">' +
            '<div id="upT"></div>' +
          '</div>' +
          '<div id="rightcross">' +
            '<div id="rightT"></div>' +
          '</div>' +
          '<div id="midcross">' +
            '<div id="midCircle"></div>' +
          '</div>' +
          '<div id="botcross">' +
            '<div id="downT"></div>' +
          '</div>' +
        '</div>' +
      '</div>' +
      '<div id="right">' +
        '<div id="stats">' +
          '<strong>Name:</strong> TO-DO@@@<br/>' +
          '<strong>Type:</strong> TO_DO@@@<br/>' +
          '<strong>Height:</strong> TO-DO@@@<br/>' +
          '<strong>Weight:</strong> TO-DO@@@@<br/><br/>' +
          '<strong>##############</strong><br/>' +
          'Insert poke description here!!' +
        '</div>' +
        '<div id="blueButtons1">' +
          '<div class="blueButton"></div>' +
          '<div class="blueButton"></div>' +
          '<div class="blueButton"></div>' +
          '<div class="blueButton"></div>' +
          '<div class="blueButton"></div>' +
        '</div>' +
        '<div id="blueButtons2">' +
          '<div class="blueButton"></div>' +
          '<div class="blueButton"></div>' +
          '<div class="blueButton"></div>' +
          '<div class="blueButton"></div>' +
          '<div class="blueButton"></div>' +
        '</div>' +
        '<div id="miniButtonGlass4"></div>' +
        '<div id="miniButtonGlass5"></div>' +
        '<div id="barbutton3"></div>' +
        '<div id="barbutton4"></div>' +
        '<div id="yellowBox1"></div>' +
        '<div id="yellowBox2"></div>' +
        '<div id="bg_curve1_right"></div>' +
        '<div id="bg_curve2_right"></div>' +
        '<div id="curve1_right"></div>' +
        '<div id="curve2_right"></div>'+
      '</div>'
      
    };

    return externals;
});