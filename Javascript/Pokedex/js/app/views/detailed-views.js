define(function() {
    var internals = {};
    var externals = {};

    internals.elements = {};


    internals.renderPokemon = function(pokemon) {
        if(internals.elements.detailedPic) {
            internals.elements.detailedPic.empty();
            internals.elements.buttons.empty();
        }
        internals.elements.pokemonCard = $(internals.createPokemonCard(pokemon));
        internals.elements.detailedPic.append(internals.elements.pokemonCard);
    };



    externals.renderView = function (error, pokemon) {
        internals.elements.detailedPic = $('#pokedex')
        internals.elements.buttons = $('#app');
        console.log(pokemon);

        if (pokemon) {
        internals.renderPokemon(pokemon);
        }
    };

    internals.createPokemonCard = function(pokemon) {
        return '<img src="' + pokemon.sprite + '">';
    };

    return externals;
});