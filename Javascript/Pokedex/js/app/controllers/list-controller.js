define(['services/pokemon-service', 'views/list-view'], function(pokemonService, listView) {
    var internals = {};
    var externals = {};
    var pokeIndex = 1;

    externals.start = function() {
        internals.bindEventHandlers();
        pokemonService.getPokemon(listView.render, pokeIndex);
    };

    internals.bindEventHandlers = function() {
        listView.bind('buttonClick', internals.onNextPokeButtonClickHandler);
        listView.bind('detailsClick', internals.onDetailsButtonClickHandler)
    };

    internals.onNextPokeButtonClickHandler = function () {
        pokeIndex++;
        pokemonService.getPokemon(listView.render, pokeIndex);
        //TO-DO: Equal the pokeindex to the current poke ID.
    }

    internals.onDetailsButtonClickHandler = function () {
        window.location.hash = '#pokedetails';
    }

    return externals;
});