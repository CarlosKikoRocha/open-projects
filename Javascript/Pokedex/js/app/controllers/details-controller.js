define(['services/poke-details-service', 'views/detailed-views'], function(detailsService, detailedViews) {
    var internals = {};
    var externals = {};

    var pokeIndex = 1;

    externals.start = function() {
        detailsService.getPokemon(detailedViews.renderView, pokeIndex);
    };

    return externals;
});