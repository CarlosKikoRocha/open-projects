define(function() {
    var internals = {};
    var externals = {};


    externals.getPokemon = function(cb, pokeIndex) {
        $.ajax({
            url: 'https://pokeapi.co/api/v2/pokemon/' + pokeIndex,
            type: 'GET',
            dataType: 'json',
            success: function(results) {
                    data = { sprite: results.sprites.other['official-artwork'].front_default}
                    cb(null, data)
            },
            error: function(request, statusText, httpError) { cb(httpError || statusText)}
            });
    };
       
 
    return externals;
});