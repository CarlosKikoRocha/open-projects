$(document).ready(function() {
var value = null;
var currency = null;

fetchData(populateCurrencies);

    $('#input').change(function(event) {
        console.log(event);
        value = event.target.value;
        fetchData(processResults);
    })

    $('select').change(function(event) {
        console.log(event);
        currency = event.target.value;
    })



    function fetchData(cb) {
        $.ajax({
            url: 'https://api.exchangeratesapi.io/latest',
            type: 'GET',
            dataType: 'json',
            success: function(results) { cb(null, results) },
            error: function(request, statusText, httpError) { cb(httpError || statusText)}
        });
    }

    function populateCurrencies(error, data) {
        if (error) {
            $('#output').append(errorMessage);
        } else {
        $('#currencies').append(populateDropdown(data));
        }
    }

function processResults(error, data) {
    if (error) {
        $('#output').append(errorMessage);
    } else {
    $('#input').append(conversion(data));
    console.log(data);
    }
}

function populateDropdown(data) {
    for (var i = 0; i < Object.keys(data.rates).length; i++) {
    $('#currencies').append('<option value="' + Object.keys(data.rates)[i] + '">' + Object.keys(data.rates)[i] + '</option>');
    }
}

function errorMessage() {
    return ("<h1>Something went wrong!</h1>");
}

function conversion(data) {
    
    var conversion = value * data.rates[currency];
    $('#output').empty();
    $('#output').append("<h1>" + conversion + " "+ currency + "</h1>");
}

});