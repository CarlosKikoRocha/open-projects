var box = document.getElementById('box');
var t1 = Date.now();

moveSync(function () {
    console.log('animation finished in ' + (Date.now() - t1) + 'ms');
});

function moveSync(cb) {
    var pos = 0;
    var id = setInterval(function frame() {
        if (pos == 350) {
            clearInterval(id);
            cb();
        } else {
            pos++
            box.style.left = pos + 'px';
        }
        
    }, 10);
    
}