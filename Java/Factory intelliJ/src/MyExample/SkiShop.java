package MyExample;

public class SkiShop {

    public Snowglider yourStyle (Enum insertBoardHere) {
        if (insertBoardHere == Style.SKI) {
            System.out.println("Have you ever tried snowboarding? It's much cooler... You must be a cat person...");
            return new Skis();
        }
        if (insertBoardHere == Style.SNOWBOARD) {
            System.out.println("That's my dawggg!!!");
            return new Snowboard();
        }
        return null;
    }
}
