package MyExample;

public class Test {

    public static void main(String[] args) {

        SkiShop theBlackDiamond = new SkiShop();

        Snowglider kiko = theBlackDiamond.yourStyle(Style.SNOWBOARD);
        Snowglider francis = theBlackDiamond.yourStyle(Style.SKI);

        kiko.slide();
        francis.slide();

    }
}
