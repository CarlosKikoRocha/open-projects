package Homework;

public class Supermarket {

    public Vegetables sell(Enum EnumVeggie) {
        if (EnumVeggie == Pickles.PEPINO) {
            System.out.println("you bought a pepino.");
            return new Pepino();
        }
        if (EnumVeggie == Pickles.CORNICHON) {
            System.out.println("you bought a cornichon.");
            return new Cornichon();
        }
        return null;
    }
}
