package org.academiadecodigo.bootcamp.gfx.simplegfx;

import org.academiadecodigo.bootcamp.grid.GridColor;
import org.academiadecodigo.bootcamp.grid.GridDirection;
import org.academiadecodigo.bootcamp.grid.position.AbstractGridPosition;
import org.academiadecodigo.bootcamp.grid.position.GridPosition;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

/**
 * Simple graphics position
 */
public class SimpleGfxGridPosition extends AbstractGridPosition {

    private Rectangle rectangle;
    private SimpleGfxGrid simpleGfxGrid;


    /**
     * Simple graphics position constructor
     *
     * @param grid Simple graphics grid
     */
    public SimpleGfxGridPosition(SimpleGfxGrid grid) {
        super((int) (Math.random() * grid.getCols()), (int) (Math.random() * grid.getRows()), grid);
        this.rectangle = new Rectangle(grid.getX(), grid.getY(), grid.getCellSize(), grid.getCellSize());
        this.rectangle.fill();
    }

    /**
     * Simple graphics position constructor
     *
     * @param col  position column
     * @param row  position row
     * @param grid Simple graphics grid
     */
    public SimpleGfxGridPosition(int col, int row, SimpleGfxGrid grid) {
        super(col, row, grid);
        this.rectangle = new Rectangle(grid.columnToX(col), grid.rowToY(row), grid.getCellSize(), grid.getCellSize());
        this.rectangle.fill();
        this.simpleGfxGrid = new SimpleGfxGrid(col, row);
    }

    /**
     * @see GridPosition#show()
     */
    @Override
    public void show() {
        this.rectangle.fill();
    }

    /**
     * @see GridPosition#hide()
     */
    @Override
    public void hide() {
        this.rectangle.delete();
    }

    /**
     * @see GridPosition#moveInDirection(GridDirection, int)
     */
    @Override
    public void moveInDirection(GridDirection direction, int distance) {
        switch (direction) {
            case RIGHT:
                if (super.getCol() + distance < simpleGfxGrid.getWidth()) {
                    this.rectangle.translate(distance * simpleGfxGrid.getCellSize(), 0);
                } else {
                    this.rectangle.translate(super.getCol(), 0);
                }
                break;
            case LEFT:
                if (super.getCol() - distance > 0) {
                    this.rectangle.translate(-distance * simpleGfxGrid.getCellSize(), 0);
                } else {
                    this.rectangle.translate(-super.getCol(), 0);
                }
                break;
            case UP:
                if(super.getRow() - distance > 0) {
                    this.rectangle.translate(0, -distance * simpleGfxGrid.getCellSize());
                } else {
                    this.rectangle.translate(0, -super.getRow());
                }
                break;
            case DOWN:
                if(super.getRow() + distance < simpleGfxGrid.getHeight()) {
                    this.rectangle.translate(0, distance * simpleGfxGrid.getCellSize());
                } else {
                    this.rectangle.translate(0, simpleGfxGrid.getHeight());
                }
                break;

        }
    }

    /**
     * @see AbstractGridPosition#setColor(GridColor)
     */
    @Override
    public void setColor(GridColor color) {
        super.setColor(color);
        switch (super.getColor()) {

            case RED:
                rectangle.setColor(Color.RED);
                break;
            case GREEN:
                rectangle.setColor(Color.GREEN);
                break;
            case BLUE:
                rectangle.setColor(Color.BLUE);
                break;
            case MAGENTA:
                rectangle.setColor(Color.MAGENTA);
                break;
            case NOCOLOR:
                rectangle.setColor(Color.WHITE);
                break;
        }
    }
}
