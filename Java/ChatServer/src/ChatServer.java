import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ChatServer {
    private ServerSocket serverSocket;
    private Vector<ClientHandler> vector;
    private ExecutorService cachedPool;


    public ChatServer() throws IOException {
        this.serverSocket = new ServerSocket(10001);
        this.vector = new Vector(2, 2);
    }

    public void broadCast(String message) {
        synchronized (this){
        for (ClientHandler client : vector) {
            client.send(message);
        }
        }
    }
    public String currentUsers() {
            String list = "Currently connected: ";
            for (int i = 0; i < vector.size(); i++) {
                if (i != 0) {
                    list += ", ";
                }
                list += vector.elementAt(i).nickname;
            }
            list += ".";
            return list;
    }


    public void init() throws IOException {
        boolean activeThread = true;
        while (activeThread) {
            this.cachedPool = Executors.newCachedThreadPool();
            cachedPool.submit(new ClientHandler(serverSocket));
        }
    }

    public class ClientHandler implements Runnable {


        private Socket clientSocket;
        private BufferedReader breader;
        private PrintWriter pWriter;
        private String defaultNickname = "Guest";
        private String nickname = defaultNickname;
        private String message;


        public ClientHandler(ServerSocket serverSocket) throws IOException {
            this.clientSocket = serverSocket.accept();
            System.out.println("client connected.." + clientSocket);
            this.breader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            this.pWriter = new PrintWriter(clientSocket.getOutputStream(), true);
            vector.add(this);

        }

        public void send(String message) {
            pWriter.println(message);
        }

        public void userList(){
            pWriter.println();
        }

        @Override
        public void run() {
            try {
                pWriter.println("hello, please set your nickname:");
                nickname = breader.readLine();
                pWriter.println("Nickname set to: " +"\"" + nickname + "\"" + " you can change at any time by typing the command \"!nickname\"."
                        + "\nLog out safely by typing \"!logout\".");
            } catch (IOException e) {
                e.printStackTrace();
            }
            while (true) {
                try {
                    this.message = breader.readLine();
                    if (message.equals("!nickname")) {
                        String oldNick = nickname;
                        nickname = breader.readLine();
                        broadCast(oldNick + " is now known as: " + nickname);
                    } else if (message.equals("!logout")) {
                        broadCast(nickname + " disconnected.");
                        System.out.println(nickname + " has disconnected.");
                        clientSocket.close();
                        cachedPool.shutdownNow();
                        vector.remove(this);
                    } else if (message.equals("!list")) {
                        pWriter.println(currentUsers());
                    } else {
                        System.out.println(message);
                        broadCast(nickname + ": " + message);
                    }
                } catch (IOException e) {
                    try {
                        clientSocket.close();
                        cachedPool.shutdownNow();
                        vector.remove(this);
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                }
            }
        }
    }

    public static void main(String[] args) {

        try {
            ChatServer chatServer = new ChatServer();
            chatServer.init();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
