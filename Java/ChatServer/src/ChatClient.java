import java.io.*;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ChatClient {
    public static void main(String[] args) {

        String hostName = "127.0.0.1";
        int portNumber = 10001;


        try {
            Socket clientSocket = new Socket(hostName, portNumber);

            BufferedReader systemIn = new BufferedReader(new InputStreamReader(System.in));
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            ExecutorService singleExecutor = Executors.newSingleThreadExecutor();
            singleExecutor.submit(new Runnable() {
                @Override
                public void run() {
                    while (true) {
                        String message = "";
                        try {
                            message = in.readLine();
                            if (message == null) {
                                clientSocket.close();
                                singleExecutor.shutdown();
                                break;
                            }
                            System.out.println(message);
                        } catch (IOException e) {
                            System.out.println("Connection with the server has been lost. Please \"!logout\" and restart.");
                            try {
                                clientSocket.close();
                                singleExecutor.shutdownNow();
                            } catch (IOException ioException) {
                                ioException.printStackTrace();
                            }
                            break;
                        }
                    }
                }
            });
            boolean active = true;
            while (active) {
                if (active) {
                    String clientMessage = systemIn.readLine();
                    if (clientMessage.equals("!logout")) {
                        active = false;
                    }
                    out.println(clientMessage);

                }
            }
        } catch (IOException e) {
            System.out.println("Unable to connect to server...");
        }
        System.out.println("Shutting down...\nSee you soon!");
    }

}
