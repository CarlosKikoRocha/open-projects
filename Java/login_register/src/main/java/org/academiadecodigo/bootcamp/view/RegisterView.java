package org.academiadecodigo.bootcamp.view;

import org.academiadecodigo.bootcamp.controller.RegisterController;
import org.academiadecodigo.bootcamp.scanners.string.PasswordInputScanner;
import org.academiadecodigo.bootcamp.scanners.string.StringInputScanner;

public class RegisterView extends AbstractView {

    private RegisterController registerController;


    @Override
    public void show() {
        System.out.println("Please input new credentials.\n");
        StringInputScanner usernameQuestion = new StringInputScanner();
        usernameQuestion.setMessage("Your Username: ");
        StringInputScanner passwordQuestion = new PasswordInputScanner();
        passwordQuestion.setMessage("Your password: ");
        String username = prompt.getUserInput(usernameQuestion);
        String password = prompt.getUserInput(passwordQuestion);
        registerController.action(username,password);
    }

    public void setRegisterController(RegisterController registerController) {
        this.registerController = registerController;
    }
}
