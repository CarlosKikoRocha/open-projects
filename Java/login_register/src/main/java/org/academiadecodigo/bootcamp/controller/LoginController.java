package org.academiadecodigo.bootcamp.controller;

import org.academiadecodigo.bootcamp.model.ServiceImplementation;
import org.academiadecodigo.bootcamp.view.View;

public class LoginController implements Controller {

    private View loginView;
    private ServiceImplementation serviceImplementation;
    private RegisterController registerController;

    @Override
    public void init() {
        loginView.show();
    }

    @Override
    public void nextController(ControllerType controllerType) {
        switch (controllerType) {
            case LOGIN -> this.init();
            case REGISTER -> registerController.init();
            case END -> System.out.println("End of program, GGWP.");
        }
    }

    @Override
    public void action(String username, String password) {
        boolean registered = serviceImplementation.authenticate(username, password);
        if (!registered) {
            registerController.init();
            nextController(ControllerType.LOGIN);
        }
    }

    public void setLoginView(View loginView) {
        this.loginView = loginView;
    }

    public void setServiceImplementation(ServiceImplementation serviceImplementation) {
        this.serviceImplementation = serviceImplementation;
    }

    public void setRegisterController(RegisterController registerController) {
        this.registerController = registerController;
    }
}
