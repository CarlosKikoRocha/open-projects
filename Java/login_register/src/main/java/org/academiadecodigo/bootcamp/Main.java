package org.academiadecodigo.bootcamp;

import org.academiadecodigo.bootcamp.controller.LoginController;
import org.academiadecodigo.bootcamp.controller.RegisterController;
import org.academiadecodigo.bootcamp.model.ServiceImplementation;
import org.academiadecodigo.bootcamp.view.LoginView;
import org.academiadecodigo.bootcamp.view.RegisterView;

public class Main {

    public static void main(String[] args) {

        Bootstrap bootstrap = new Bootstrap();

        LoginController loginController = bootstrap.start();

        loginController.init();

    }
}
