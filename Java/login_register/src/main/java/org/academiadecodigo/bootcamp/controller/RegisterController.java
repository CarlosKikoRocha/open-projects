package org.academiadecodigo.bootcamp.controller;

import org.academiadecodigo.bootcamp.controller.Controller;
import org.academiadecodigo.bootcamp.model.ServiceImplementation;
import org.academiadecodigo.bootcamp.view.RegisterView;
import org.academiadecodigo.bootcamp.view.View;

public class RegisterController implements Controller {

    private View registerView;
    private ServiceImplementation serviceImplementation;
    private LoginController loginController;

    @Override
    public void init() {
        registerView.show();
    }

    @Override
    public void nextController(ControllerType controllerType) {
        System.out.println("enum.");
        switch (controllerType) {
            case LOGIN -> loginController.init();
            case REGISTER -> this.init();
        }
    }

    @Override
    public void action(String username, String password) {
        serviceImplementation.register(username, password);
    }

    public void setRegisterView(View registerView) {
        this.registerView = registerView;
    }

    public void setServiceImplementation(ServiceImplementation serviceImplementation) {
        this.serviceImplementation = serviceImplementation;
    }
}
