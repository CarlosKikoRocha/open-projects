package org.academiadecodigo.bootcamp.model;

import org.academiadecodigo.bootcamp.controller.Controller;
import org.academiadecodigo.bootcamp.view.View;

import java.util.HashMap;

public class ServiceImplementation implements Service {

    private HashMap <String, String> customers = new HashMap<>();



    public boolean authenticate(String username, String password) {
        if(customers.containsKey(username)
                && customers.containsValue(password)) {
            System.out.println("Login successful");
            return true;
        }
        System.out.println("You don't seem to be a valid user, please register first: ");
        return false;

    }

    public void register(String username, String password) {
        customers.put(username, password);
        System.out.println("Registered " + username + " successfully.");
    }

    public HashMap<String, String> getCustomers() {
        return customers;
    }

}
