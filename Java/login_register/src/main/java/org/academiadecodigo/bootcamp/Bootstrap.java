package org.academiadecodigo.bootcamp;

import org.academiadecodigo.bootcamp.controller.LoginController;
import org.academiadecodigo.bootcamp.controller.RegisterController;
import org.academiadecodigo.bootcamp.model.ServiceImplementation;
import org.academiadecodigo.bootcamp.view.LoginView;
import org.academiadecodigo.bootcamp.view.RegisterView;

public class Bootstrap {

    public LoginController start() {

        LoginView loginView = new LoginView();
        RegisterView registerView = new RegisterView();

        Prompt prompt = new Prompt(System.in, System.out);

        LoginController loginController = new LoginController();
        RegisterController registerController = new RegisterController();
        ServiceImplementation serviceImplementation = new ServiceImplementation();


        loginView.setPrompt(prompt);
        registerView.setPrompt(prompt);

        loginView.setLoginController(loginController);
        registerView.setRegisterController(registerController);

        loginController.setLoginView(loginView);
        loginController.setRegisterController(registerController);
        loginController.setServiceImplementation(serviceImplementation);

        registerController.setRegisterView(registerView);
        registerController.setServiceImplementation(serviceImplementation);

        return loginController;
    }
}
