package org.academiadecodigo.bootcamp.view;

import org.academiadecodigo.bootcamp.controller.LoginController;
import org.academiadecodigo.bootcamp.scanners.string.PasswordInputScanner;
import org.academiadecodigo.bootcamp.scanners.string.StringInputScanner;

public class LoginView extends AbstractView {


    private LoginController loginController;



    public void show() {
        StringInputScanner usernameQuestion = new StringInputScanner();
        usernameQuestion.setMessage("Username: ");
        StringInputScanner passwordQuestion = new PasswordInputScanner();
        passwordQuestion.setMessage("Password: ");
        String username = prompt.getUserInput(usernameQuestion);
        String password = prompt.getUserInput(passwordQuestion);
        loginController.action(username, password);
    }


    public void setLoginController(LoginController loginController) {
        this.loginController = loginController;
    }
}
