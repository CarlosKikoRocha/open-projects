package org.academiadecodigo.bootcamp.controller;

public interface Controller {

    public void init();

    public void nextController(ControllerType controllerType);

    public void action(String username, String password);
}
