package org.academiadecodigo.bootcamp.controller;

public enum ControllerType {

    LOGIN,
    REGISTER,
    END;
}
