package ExercícioTCP;

import jdk.swing.interop.SwingInterOpUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLOutput;

public class ChatServer {

    public static void main(String[] args) {
        int portNumber = 9996;
        boolean active = true;

        try {

            ServerSocket serverSocket = new ServerSocket(portNumber);
            Socket clientSocket = serverSocket.accept();
            System.out.println("New client connected.");    //When client connects.

            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            while(active) {

                String message = in.readLine();
                System.out.println(message);
                if (message.equals("disconnect")) {
                    out.println("You have been disconnected.");
                    active = false;
                }
            }
            clientSocket.close();
            serverSocket.close();
            System.out.println("Server off.");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
