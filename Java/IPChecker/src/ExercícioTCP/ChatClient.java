package ExercícioTCP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class ChatClient {

    public static void main(String[] args) {




        String hostName = "127.0.0.1";
        int portNumber = 9996;
        boolean active = true;

        try {
            System.out.print("Please type your nickname: ");

            InputStreamReader nickReader = new InputStreamReader(System.in);
            BufferedReader nickBReader = new BufferedReader(nickReader);

            String nickname = nickBReader.readLine();

            System.out.println("Trying to establish a connection, please wait...\nType \"disconnect\" to log out." +
                    "\nType \"change nick\" to change nickname.");

            Socket clientSocket = new Socket(hostName, portNumber);


            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            while(active) {
                InputStreamReader reader = new InputStreamReader(System.in);
                BufferedReader bReader = new BufferedReader(reader);

                System.out.print(nickname + ": ");
                String yourMessage = bReader.readLine();
                if (yourMessage.equals("disconnect")) {
                    out.println(yourMessage);
                    String serverReply = in.readLine();
                    System.out.println(serverReply);
                    active = false;
                }
                if (yourMessage.equals("change nick")){
                    System.out.print("new nick: ");
                    nickname = bReader.readLine();
                }
                out.println(yourMessage);
            }
            clientSocket.close();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
