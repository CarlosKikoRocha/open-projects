package URLExercise;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class SourceViewer {

    public static void main(String[] args) {


        try {
            URL url = new URL("http://www.google.com");
            //InputStream in = new BufferedInputStream(url.openStream());
            //InputStreamReader reader = new InputStreamReader(in);
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

            URLConnection urlConnection = url.openConnection();

            String urlProtocol = url.getProtocol();

            Object urlcontent = url.getContent();

            InputStream urlStream = url.openStream();

            String source = in.readLine();
            System.out.println(source);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
