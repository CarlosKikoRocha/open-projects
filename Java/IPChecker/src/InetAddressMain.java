import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class InetAddressMain {

    public static void main(String[] args) {

        String website = "www.academiadecodigo.org";

        InetAddress webbers = null;


        try {
            webbers = InetAddress.getByName(website);

        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        try {
            System.out.println(webbers.isReachable(400));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
