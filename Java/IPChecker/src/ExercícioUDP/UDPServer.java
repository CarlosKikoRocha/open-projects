package ExercícioUDP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class UDPServer {

    public static void main(String[] args) {

        InputStreamReader reader = new InputStreamReader(System.in);
        BufferedReader bReader = new BufferedReader(reader);

        System.out.print("Port? ");
        try {
            String portAnswear = bReader.readLine();


            String hostName = "localhost";
            int portNumber = Integer.parseInt(portAnswear);

            byte[] sendBuffer;
            byte[] recvBuffer = new byte[1024];


            DatagramSocket socket = new DatagramSocket(portNumber);

            DatagramPacket receivePacket = new DatagramPacket(recvBuffer, recvBuffer.length);
            socket.receive(receivePacket);
            System.out.println("teste servidor");
            recvBuffer = receivePacket.getData();

            String string = new String(recvBuffer);

            String caps = string.toUpperCase();

            sendBuffer = caps.getBytes();

            DatagramPacket sendPacket = new DatagramPacket(sendBuffer, sendBuffer.length, InetAddress.getByName(hostName), receivePacket.getPort());
            socket.send(sendPacket);


            socket.close();

            System.out.println(string);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
