package ExercícioUDP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;

public class UDPClient {

    public static void main(String[] args) {

        InputStreamReader reader = new InputStreamReader(System.in);
        BufferedReader bReader = new BufferedReader(reader);

        System.out.print("What's your IP? ");
        try {
            String IPAnswear = bReader.readLine();
            System.out.print("Port? ");
            String portAnswear = bReader.readLine();
            System.out.print("Your message: ");
            String message = bReader.readLine();


            String hostName = IPAnswear;
            int portNumber = Integer.parseInt(portAnswear);

            byte[] sendBuffer;   //the message to be sent.
            byte[] recvBuffer = new byte[1024];

            sendBuffer = message.getBytes();

            //System.out.println(sendBuffer);
            DatagramSocket socket = new DatagramSocket(portNumber);

            DatagramPacket sendPacket = new DatagramPacket(sendBuffer, sendBuffer.length, InetAddress.getByName(hostName), 9998);
            socket.send(sendPacket);

            //This is where I receive the information provided.

            DatagramPacket receivePacket = new DatagramPacket(recvBuffer, recvBuffer.length);
            socket.receive(receivePacket);

            recvBuffer = receivePacket.getData();

            String text = new String(recvBuffer);

            socket.close();

            System.out.println(text);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
