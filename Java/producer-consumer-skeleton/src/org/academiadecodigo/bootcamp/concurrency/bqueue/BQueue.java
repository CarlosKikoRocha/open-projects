package org.academiadecodigo.bootcamp.concurrency.bqueue;

import java.util.LinkedList;

/**
 * Blocking Queue
 * @param <T> the type of elements stored by this queue
 */
public class BQueue<T> {

    private final int limit;
    private int currentCapacity;
    private LinkedList<T> linkedList;

    /**
     * Constructs a new queue with a maximum size
     * @param limit the queue size
     */
    public BQueue(int limit) {
        this.limit = limit;
        this.currentCapacity = 0;
        linkedList = new LinkedList<>();
        }

    /**
     * Inserts the specified element into the queue
     * Blocking operation if the queue is full
     * @param data the data to add to the queue
     */
    public void offer(T data) throws InterruptedException {
        synchronized (this) {
            Thread.sleep(250);
            while (getSize() == getLimit()) {
                this.wait();
            }
            linkedList.add(data);
            this.currentCapacity++;
            notifyAll();
            System.out.println(Thread.currentThread() + " added a " + data + ", current size = " + getSize());
        }
    }

    /**
     * Retrieves and removes data from the head of the queue
     * Blocking operation if the queue is empty
     * @return the data from the head of the queue
     */
    public T poll() throws InterruptedException {
        synchronized (this) {
            Thread.sleep(250);
            while (currentCapacity == 0) {
                this.wait();
            }
            notifyAll();
            currentCapacity--;
            System.out.println(Thread.currentThread() + " removed the " + linkedList.peek() + ", current size = " + (getSize() - 1));
            return linkedList.remove();
        }
    }

    /**
     * Gets the number of elements in the queue
     * @return the number of elements
     */
    public int getSize() {
        return linkedList.size();
    }

    /**
     * Gets the maximum number of elements that can be present in the queue
     * @return the maximum number of elements
     */
    public int getLimit() {
        return limit;
    }

}
