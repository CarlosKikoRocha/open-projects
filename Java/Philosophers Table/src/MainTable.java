public class MainTable {
    public static void main(String[] args) {


        ChopstickColor red = new ChopstickColor("red");
        ChopstickColor blue = new ChopstickColor("blue");
        ChopstickColor pink = new ChopstickColor("pink");
        ChopstickColor green = new ChopstickColor("green");
        ChopstickColor yellow = new ChopstickColor("yellow");



        Philosopher orangePhil = new Philosopher(new Dish(10), red, yellow, "orange");
        Thread t1 = new Thread(orangePhil);
        t1.setName("orange");

        Philosopher limePhil = new Philosopher(new Dish(10), yellow, green, "lime");
        Thread t2 = new Thread(limePhil);
        t2.setName("lime");

        Philosopher brownPhil = new Philosopher(new Dish(10), green, pink, "brown");
        Thread t3 = new Thread(brownPhil);
        t3.setName("brown");

        Philosopher violetPhil = new Philosopher(new Dish(10), pink, blue, "violet");
        Thread t4 = new Thread(violetPhil);
        t4.setName("violet");

        Philosopher purplePhil = new Philosopher(new Dish(10), blue, red, "purple");
        Thread t5 = new Thread(purplePhil);
        t5.setName("purple");


        t1.start(); t2.start(); t3.start(); t4.start(); t5.start();
    }
}
