public class Philosopher implements Runnable {

    private boolean thinking;
    private boolean eating;
    private Dish dish;
    private Chopstick right;
    private Chopstick left;
    private String color;

    public Philosopher(Dish dish, Chopstick right, Chopstick left, String philColor) {
        this.dish = dish;
        this.right = right;
        this.left = left;
        this.color = philColor;
    }


    @Override
    public synchronized void run(Philosopher this) {
        //System.out.println("test " + dish.getDosesLeft() + " meals left for " + Philosopher.this.toString());
        while (dish.getDosesLeft() > 0) {
            try {
                while (this.right.isBeingUsed() == true || this.left.isBeingUsed() == true) {
                    Thread.sleep(1000);
                    setThinking();
                }
                synchronized (this.right) {
                    synchronized (this.left) {
                        while (this.right.isBeingUsed() == false && this.left.isBeingUsed() == false) {
                            Thread.sleep(1000);
                            setEating();
                        }
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
        System.out.println("\n" + Philosopher.this.toString() + " has finished his meal.\n");
    }

    public boolean isThinking() {
        return thinking;
    }

    public void setThinking() {
        System.out.println(Philosopher.this.toString() + ": Wait... I'm thinking! erhm.. How do you do this...");
        this.thinking = true;
        this.eating = false;
        this.right.setBeingUsed(false);
        this.left.setBeingUsed(false);
    }

    public boolean isEating() {
        return eating;
    }

    public void setEating() {
        this.thinking = false;
        this.right.setBeingUsed(true);
        this.left.setBeingUsed(true);
        dish.eatDose();
        System.out.println(Philosopher.this.toString() + " is eating with " +
                right.toString() + " and " +
                left.toString() +
                ". Yum! " + dish.getDosesLeft() + " left.");
        this.eating = true;
    }

    @Override
    public String toString() {
        return color + "Phil";
    }
}
