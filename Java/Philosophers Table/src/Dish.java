public class Dish {


    private final int doses;
    private int dosesLeft;


    public Dish(int doses){
        this.doses = doses;
        this.dosesLeft = doses;
    }

    public void eatDose(){
        if (dosesLeft == 0){
            return;
        }
        dosesLeft--;
    }

    public int getDosesLeft() {
        return dosesLeft;
    }
}
