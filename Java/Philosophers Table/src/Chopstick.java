public abstract class Chopstick {

    private boolean beingUsed = false;
    private String color;

    public Chopstick(String color) {
        this.color = color;
    }
    public void setBeingUsed(boolean beingUsed) {
        this.beingUsed = beingUsed;
    }

    public boolean isBeingUsed() {
        return beingUsed;
    }

    @Override
    public String toString() {
        return color + "Chopstick";
    }
}
