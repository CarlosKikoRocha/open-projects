public class Main {

    public static void main(String[] args) {


        BiOperation add = ((i1, i2) -> i1 + i2);

        BiOperation subtract = ((i1, i2) -> i1 - i2);

        BiOperation multiply = ((i1, i2) -> i1 * i2);

        BiOperation divide = ((i1, i2) -> i1 / i2);

        double addResult = add.calculator(2, 3);    //Hardcode test.

        MonoOperation square = ((i1 -> multiply.calculator(i1, i1)));

        MonoOperation sqrRoot = (i1 -> Math.sqrt(i1));

        double rootResult = sqrRoot.calculator(21);

        System.out.println(rootResult);


    }
}
