public interface BiOperation {

    double calculator(int i1, int i2);
}
