package server;


import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    private int portNumber = 9080;
    private BufferedReader bReader;
    private DataOutputStream dataOutStream;
    private DataInputStream dataInStream;
    private File toBeSent;
    private byte[] buffer;
    private String command;
    private String httpHeader;

    public void initServer() throws IOException {

        ServerSocket serverSocket = new ServerSocket(portNumber);     //open the server socket

        while (true) {
            Socket clientSocket = serverSocket.accept();      //establish a connection
            Thread coolThread = new Thread(new NewThread());
            coolThread.start();
            System.out.println(coolThread.currentThread().getName());

            //reader for the info sent by the browser
            bReader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            //the channel to send goodies
            dataOutStream = new DataOutputStream(clientSocket.getOutputStream());
            command = bReader.readLine();//                ---> Need the first line to know the request.

            setFile();
            dataInStream = new DataInputStream(new FileInputStream(toBeSent));

            dataInStream.read(buffer);

            System.out.println(command);

            String httpMessage = bReader.readLine();     //read info sent from the browser

            /*while (!httpMessage.isEmpty()) {
                System.out.println(httpMessage);
                httpMessage = bReader.readLine();
            }

             */
            //What to do with the request is encapsulated at send().
            send();
            dataInStream.close();
            //System.out.println("TEST");
            //close the connection hence why Http is stateless
            System.out.println("end of thread");
            clientSocket.close();
        }


    }

    public void setFile() {
        switch (command) {
            case "GET / HTTP/1.1":
                toBeSent = new File("www/Greeting.html");
                buffer = new byte[(int) toBeSent.length()];
                break;
            case "GET /favicon.ico HTTP/1.1":
                toBeSent = new File("www\\pepino.ico");
                buffer = new byte[(int) toBeSent.length()];
                break;
            case "GET /more HTTP/1.1":
                toBeSent = new File("www/more/SomeImage.html");
                buffer = new byte[(int) toBeSent.length()];
                break;
            case "GET /IMG_0443.jpg HTTP/1.1":
                toBeSent = new File("www\\more\\IMG_0443.jpg");
                buffer = new byte[(int) toBeSent.length()];
                break;

        }
    }

    public void send() throws IOException {
        switch (command) {
            case "GET / HTTP/1.1":
                httpHeader = "HTTP/1.0 200 OK Content-Type: text/html; charset=UTF-8 Content-Length: " + toBeSent.length() + "\r\n\r\n";
                dataOutStream.write(httpHeader.getBytes());
                dataOutStream.flush();
                dataOutStream.write(buffer);
                dataOutStream.flush();
                break;
            case "GET /favicon.ico HTTP/1.1":
                httpHeader = "HTTP/1.0 200 OK Content-Type: image/x-icon; charset=UTF-8 Content-Length: " + toBeSent.length() + "\r\n\r\n";
                dataOutStream.write(httpHeader.getBytes());
                dataOutStream.flush();
                dataOutStream.write(buffer);
                dataOutStream.flush();
                break;
            case "GET /more HTTP/1.1":
                httpHeader = "HTTP/1.0 200 OK Content-Type: text.html; charset=UTF-8 Content-Length: " + toBeSent.length() + "\r\n\r\n";
                dataOutStream.write(httpHeader.getBytes());
                dataOutStream.flush();
                dataOutStream.write(buffer);
                dataOutStream.flush();
                break;
            case "GET /IMG_0443.jpg HTTP/1.1":
                httpHeader = "HTTP/1.0 200 OK Content-Type: images/jpeg; Content-Length: " + toBeSent.length() + "\r\n\r\n";
                dataOutStream.write(httpHeader.getBytes());
                dataOutStream.flush();
                dataOutStream.write(buffer);
                dataOutStream.flush();
                break;
        }
    }
}