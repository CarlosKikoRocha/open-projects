package org.academiadecodigo.bootcamp;

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

public class User implements KeyboardHandler {

    private Pen myPen;
    private Keyboard keyboard;
    private Grid grid;

    public User(Pen pen, Grid grid) {
        keyboard = new Keyboard(this);
        myPen = pen;
        this.grid = grid;

    }

    public void userInit() {
        KeyboardEvent inputLeft = new KeyboardEvent();
        inputLeft.setKey(KeyboardEvent.KEY_LEFT);
        inputLeft.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(inputLeft);

        KeyboardEvent inputRight = new KeyboardEvent();
        inputRight.setKey(KeyboardEvent.KEY_RIGHT);
        inputRight.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(inputRight);

        KeyboardEvent inputUp = new KeyboardEvent();
        inputUp.setKey(KeyboardEvent.KEY_UP);
        inputUp.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(inputUp);

        KeyboardEvent inputDown = new KeyboardEvent();
        inputDown.setKey(KeyboardEvent.KEY_DOWN);
        inputDown.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(inputDown);

        KeyboardEvent draw = new KeyboardEvent();
        draw.setKey(KeyboardEvent.KEY_SPACE);
        draw.setKeyboardEventType((KeyboardEventType.KEY_PRESSED));
        keyboard.addEventListener(draw);

        KeyboardEvent clear = new KeyboardEvent();
        clear.setKey(KeyboardEvent.KEY_C);
        clear.setKeyboardEventType((KeyboardEventType.KEY_PRESSED));
        keyboard.addEventListener(clear);
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {

        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_LEFT:
                myPen.moveLeft();
                break;
            case KeyboardEvent.KEY_RIGHT:
                myPen.moveRight();
                break;
            case KeyboardEvent.KEY_UP:
                myPen.moveUp();
                break;
            case KeyboardEvent.KEY_DOWN:
                myPen.moveDown();
                break;
            case KeyboardEvent.KEY_SPACE:
                grid.paintArrayCell();
                break;
            case KeyboardEvent.KEY_C:
                grid.clearGrid();
                break;
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

    }

}