package org.academiadecodigo.bootcamp;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Cell {


    private int width;
    private int height;
    private Rectangle square;
    private boolean fill = false;
    private int xAxis;
    private int yAxis;

    public Cell(int xAxis, int yAxis) {
        this.width = 25;
        this.height = 25;
        this.xAxis = xAxis;
        this.yAxis = yAxis;
        square = new Rectangle(xAxis, yAxis, width, height);
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public int getxAxis() {
        return xAxis;
    }

    public int getyAxis() {
        return yAxis;
    }

    public void initCell() {
        square.draw();
    }

    public void setFill() {
        if (!fill) {
            square.setColor(Color.RED);
            square.fill();
            this.fill = true;
        } else {
            square.setColor(Color.BLACK);
            square.draw();
            this.fill = false;
        }
    }

    public void reset() {
        square.setColor(Color.BLACK);
        square.draw();
        this.fill = false;
    }
}

