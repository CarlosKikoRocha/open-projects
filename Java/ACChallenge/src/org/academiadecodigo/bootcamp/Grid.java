package org.academiadecodigo.bootcamp;

import java.io.*;

public class Grid {

    public static final int PADDING = 10;
    private Cell[] background;
    private int xAxis = PADDING;
    private int yAxis = PADDING;
    private Pen myPen;

    public Grid() {
        myPen = new Pen();
        this.background = new Cell[1200];

        for (int i = 0; i < background.length; i++) {
            int j = i + 1;
            background[i] = new Cell(xAxis, yAxis);
            System.out.println(getyAxis() + " " + getxAxis());
            xAxis += 25;

            if (j % 40 == 0) {
                yAxis += 25;
                xAxis = PADDING;
            }

        }
    }


    public int getyAxis() {
        return yAxis;
    }

    public int getxAxis() {
        return xAxis;
    }

    public Cell[] getBackground() {
        return background;
    }

    //paints square hovered by pen
    public void paintArrayCell() {
        Cell cell = null;
        for (int i = 0; i < background.length; i++) {
            System.out.println(myPen.getxAxis());
            if (background[i].getyAxis() == myPen.getyAxis() &&
                    background[i].getxAxis() == myPen.getxAxis()) {
                cell = background[i];
                cell.setFill();
            }
        }
    }

    public void clearGrid() {
        for (int i = 0; i < background.length; i++) {
            background[i].reset();
        }
    }

    public Pen getMyPen() {
        return myPen;
    }

    public void gridInit() {
        for (int i = 0; i < background.length; i++) {
            background[i].initCell();
        }
    }
        //Tentativa de save
    public void save() {
        File path = new File(background.toString());
        File saveFile = new File("resources/yourSave.txt");
        try {
            InputStream in = new BufferedInputStream(new FileInputStream(path));
            OutputStream out = new BufferedOutputStream(new FileOutputStream(saveFile));

            byte[] buffer = new byte[64];
            int lengthRead;
            while((lengthRead = in.read(buffer)) > 0) {
                out.write(buffer,0,lengthRead);
                out.flush();
                out.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
