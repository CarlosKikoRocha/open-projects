package org.academiadecodigo.bootcamp;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Pen {

    private int width;
    private int height;
    private Rectangle myPen;
    private int xAxis = Grid.PADDING;
    private int yAxis = Grid.PADDING;
    private Cell sq;

    public Pen() {
        this.width = 25;
        this.height = 25;

        myPen = new Rectangle(xAxis, yAxis, width, height);
    }

    public int getyAxis() {
        return yAxis;
    }
    public int getxAxis() {
        return xAxis;
    }



    public void initPen() {
        myPen.draw();
        myPen.setColor(Color.GREEN);
        myPen.fill();
    }

    public void paint() {
    }

    public void moveLeft() {
        if (xAxis <= Grid.PADDING) {
            this.xAxis = Grid.PADDING;
            myPen.translate(0, 0);
            return;
        }
        this.xAxis -= 25;
        myPen.translate(-25, 0);
    }

    public void moveRight() {
        if (xAxis >= 985) {
            this.xAxis = 985;
            myPen.translate(0, 0);
            return;
        }
        this.xAxis += 25;
        System.out.println(getxAxis());
        myPen.translate(25, 0);
    }

    public void moveUp() {
        if (yAxis <= Grid.PADDING) {
            this.yAxis = Grid.PADDING;
            myPen.translate(0,0);
            return;
        }
        this.yAxis -= 25;
        myPen.translate(0, -25);

    }

    public void moveDown() {
        if (yAxis >= 735) {
            this.yAxis = 735;
            myPen.translate(0, 0);
            return;
        }
        this.yAxis += 25;
        myPen.translate(0, 25);
    }
}
