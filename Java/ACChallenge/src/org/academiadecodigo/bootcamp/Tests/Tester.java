package org.academiadecodigo.bootcamp.Tests;

import org.academiadecodigo.bootcamp.Grid;
import org.academiadecodigo.bootcamp.Pen;
import org.academiadecodigo.bootcamp.User;

public class Tester {
    public static void main(String[] args) {

        Grid base = new Grid();
        base.gridInit();

        Pen testingPen = base.getMyPen();
        testingPen.initPen();


        User painter = new User(testingPen, base);
        painter.userInit();

        //base.save(); // test
    }
}
