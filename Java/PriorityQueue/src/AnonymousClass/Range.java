package AnonymousClass;

import java.util.Iterator;

public class Range implements Iterable<Integer> {

    private int min;
    private int max;

    private boolean ascending;

    public Range(int min, int max) {
        this.min = min;
        this.max = max;
    }

    public void setAscending(boolean ascending) {
        this.ascending = ascending;
    }


    @Override
    public Iterator<Integer> iterator() {
        if (ascending) {
            return ascendingIterator();
        } else {
            return descendingIterator();
        }
    }


    public Iterator<Integer> ascendingIterator() {
        return new Iterator() {
            int count = min - 1;

            public boolean hasNext() {
                return count < max;
            }


            public Integer next() {
                count++;
                return count;
            }
        };
    }

    public Iterator<Integer> descendingIterator() {
        return new Iterator() {
            int count = max + 1;


            public boolean hasNext() {
                return count > min;
            }

            public Integer next() {
                count--;
                return count;
            }
        };
    }
}



/*return new Iterator() {
        int count = max + 1;

@Override
public boolean hasNext() {
        return count > min;
        }
@Override
public Integer next() {
        count --;
        return count;
        }
        };*/

