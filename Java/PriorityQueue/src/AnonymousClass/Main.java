package AnonymousClass;

import java.util.Iterator;

public class Main {
    public static void main(String[] args) {

        Range r = new Range(-5, 5);

        r.setAscending(true);

        Iterator<Integer> it = r.iterator();

        for (Integer i : r) {
            System.out.println(i);

        }
    }
}
