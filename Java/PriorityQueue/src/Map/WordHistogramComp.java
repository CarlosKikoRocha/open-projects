package Map;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class WordHistogramComp implements Iterable<String> {

    public HashMap <String, Integer> hashMap = new HashMap();

    public WordHistogramComp(String text){
        String[] textArray = text.split(" ");
        for (int i = 0; i < textArray.length; i++) {
            hashMap.put(textArray[i], hashMap.containsKey(textArray[i]) ? hashMap.get(textArray[i]) + 1 : 1);
        }
    }

    public int get(String word) {
        return hashMap.get(word);
    }

    public int size() {
        return hashMap.size();
    }

    @Override
    public Iterator<String> iterator() {
        return hashMap.keySet().iterator();
    }
}
