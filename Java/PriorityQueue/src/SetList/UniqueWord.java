package SetList;

import java.util.Iterator;
import java.util.TreeSet;

public class UniqueWord implements Iterable <String>{

    public TreeSet<String> kikoSetList = new TreeSet();

    public UniqueWord(String text) {
        String[] textArray = text.split(" ");
        for (int i = 0; i < textArray.length; i++){
            kikoSetList.add(textArray[i]);
        }
    }


    @Override
    public Iterator <String> iterator() {
        return kikoSetList.iterator();
    }
}
