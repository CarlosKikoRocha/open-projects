package PriorityQueue;

import PriorityQueue.Importance;

public class Main {

    public static void main(String[] args) {

        ToDoList kikoList = new ToDoList();

        kikoList.add(Importance.MEDIUM, 1, "Get a motorbike.");
        kikoList.add(Importance.LOW, 1, "mooo");
        kikoList.add(Importance.HIGH, 1, "Snowboard!");
        kikoList.add(Importance.LOW, 2, "Be a dad.");
        kikoList.add(Importance.MEDIUM, 2, "yes.");
        kikoList.add(Importance.HIGH, 2, "Snowboard again!");

        while (!kikoList.isEmpty()) {
            System.out.println(kikoList.remove());
        }

    }
}
