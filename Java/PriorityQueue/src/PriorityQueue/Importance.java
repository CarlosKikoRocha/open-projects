package PriorityQueue;

public enum Importance {

    HIGH,
    MEDIUM,
    LOW;

}
