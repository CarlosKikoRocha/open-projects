package PriorityQueue;

import java.util.PriorityQueue;

public class ToDoList {

    private PriorityQueue <ToDoItem> queue = new PriorityQueue();

    public ToDoList() {
    }

    public void add(Importance importance, int priority, String text) {
        ToDoItem toDoItem = new ToDoItem(importance, priority, text);
        queue.add(toDoItem);
    }

    public ToDoItem remove() {
        return queue.remove();
    }

    public boolean isEmpty() {
        return queue.isEmpty();
    }
}
