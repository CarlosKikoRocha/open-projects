package PriorityQueue;

public class ToDoItem implements Comparable <ToDoItem> {

    private Importance importance;
    private int priority;
    private String item;


    public ToDoItem(Importance importance, int priority, String text) {
        this.importance = importance;
        this.item = text;
        this.priority = priority;
    }

    @Override
    public int compareTo(ToDoItem o) {
        

        if (this.importance.compareTo(o.importance) == 1) {
            return 1;
        }
        if (this.importance.compareTo(o.importance) == -1) {
            return -1;
        }
        if (this.importance.compareTo(o.importance) == 0) {
            if (this.priority - o.priority == 1){
                return 1;
            }
            if (this.priority - o.priority == -1){
                return -1;
            }
            if (this.priority - o.priority == 0){
                if (this.item.compareTo(o.item) == 1){
                    return 1;
                }
                if (this.item.compareTo(o.item) == -1){
                    return -1;
                }
                if (this.item.compareTo(o.item) == 0){
                    return 0;
                }
            }
        }
        return 0;
    }

    @Override
    public String toString() {
        return "PriorityQueue.ToDoItem{" +
                "importance=" + importance +
                ", priority=" + priority +
                ", item='" + item + '\'' +
                '}';
    }
}
