package WordReader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

public class WordReader implements Iterable<String> {

    private FileReader fileReader;
    private BufferedReader bReader;
    private String line;
    private String result = "";
    private String[] singleWord;
    private String currentLine;
    private String cachedLine;


    public WordReader(String yourTxt) {
        try {

            this.fileReader = new FileReader(yourTxt);
            this.bReader = new BufferedReader(fileReader);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


    @Override
    public Iterator<String> iterator(){
        return new Iterator<String>() {
            @Override
            public boolean hasNext() {
                boolean finished = false;
                if (!finished) {
                    try {
                        if ((line = bReader.readLine()) != null) {
                            singleWord = line.split(" ");
                            return true;
                        }

                    } catch (IOException e) {
                        finished = true;
                        System.out.println("Reading process over.");
                    }
                }
                try {
                    bReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return false;
            };

            @Override
            public String next() {
                    String currentLine = line;  //unused so far.
                    line = "";
                    for (int i = 0; i < singleWord.length; i++) {
                        line += singleWord[i] + "\n";
                        if (i == singleWord.length -1 ) {
                            line += "END OF ITERATED WORD";
                        }
                    }
                    return line;
            }
        };
    }
}