package CPExercise;

import org.w3c.dom.ls.LSOutput;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws FileNotFoundException, SecurityException {


        FileInputStream inputStream = new FileInputStream("resources/cute.jpg");
        FileOutputStream outputStream = new FileOutputStream("resources/cute2.jpg");

        try {

            byte[] buffer = new byte[2048];

            int num = 0;
            while (num != -1) {
                num = inputStream.read(buffer);

                if (num != -1) {
                    System.out.println("Bytes read: " + num);
                }


                outputStream.write(buffer);



            }
            inputStream.close();
            outputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }




    }
}
